# This file is a template, and might need editing before it works on your project.
FROM ubuntu:20.04

# RUN cd ~ \
#     && wget https://apt.kitware.com/kitware-archive.sh \
#     && bash ./kitware-archive.sh

RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt install -y --no-install-recommends \
        git \
        cmake \
        ninja-build \
        gperf \
        ccache \
        dfu-util \
        device-tree-compiler \
        wget \
        python3-dev \
        python3-pip \
        python3-setuptools \
        python3-tk \
        python3-wheel \
        xz-utils file \
        make \
        gcc \
        gcc-multilib \
        g++-multilib \
        libsdl2-dev \
    && rm -rf /var/lib/apt/lists/*

# Install west
RUN pip install west

RUN mkdir /workspace

# Get the Zephyr source code
RUN west init /workspace/zephyrproject
WORKDIR /workspace/zephyrproject
RUN west update

# Export a Zephyr CMake package. This allows CMake to automatically
# load boilerplate code required for building Zephyr applications.
RUN west zephyr-export

# Zephyr’s scripts/requirements.txt file declares additional Python
# dependencies. Install them with pip3.
RUN pip install --no-cache-dir -r zephyr/scripts/requirements.txt

# Download the latest SDK installer
RUN cd /tmp \
    && wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.13.0/zephyr-sdk-0.13.0-linux-x86_64-setup.run

# Run the installer, installing the SDK in ~/zephyr-sdk-0.13.0
RUN cd /tmp \
    && chmod +x zephyr-sdk-0.13.0-linux-x86_64-setup.run \
    && ./zephyr-sdk-0.13.0-linux-x86_64-setup.run -- -d /opt/zephyr-sdk-0.13.0 \
    && rm -f zephyr-sdk-0.13.0-linux-x86_64-setup.run

# Install udev rules
RUN cp /opt/zephyr-sdk-0.13.0/sysroots/x86_64-pokysdk-linux/usr/share/openocd/contrib/60-openocd.rules /etc/udev/rules.d \
    && udevadm control --reload
